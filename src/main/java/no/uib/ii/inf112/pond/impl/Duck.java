package no.uib.ii.inf112.pond.impl;

import no.uib.ii.inf112.pond.Pond;
import no.uib.ii.inf112.pond.PondObject;
import no.uib.ii.inf112.pond.Position;

/**
 * A duck class – recipe for making duck objects.
 * 
 * Describes the common aspects of and behaviour of duck objects (in methods)
 * and the individual differences between ducks (in field variables).
 */
public class Duck implements PondObject {
	protected int stepCount = 0;
	protected double size;
	protected Position pos;
	protected int childCount = 0;

	public void step(Pond pond) {
		if (stepCount == 24 && this.getSize() > 20) {
			stepCount = 0;
			childCount++;
			double x = pond.objs.get(childCount - 1).getX();
			double y = pond.objs.get(childCount - 1).getY();

			Position newPos = Position.create((x-1), y);
			pond.add(new Duckling(newPos, 11));
		}
		pos.move(-1, 0);
		stepCount++;
	}

	public Duck(Position pos, double size) {
		this.pos = pos;
		this.size = size;
	}

	/**
	 * @return The duck's size;
	 */
	public double getSize() {
		return size;
	}

	/**
	 * @return Current X position
	 */
	public double getX() {
		return pos.getX();
	}

	/**
	 * @return Current Y position
	 */
	public double getY() {
		return pos.getY();
	}

	@Override
	public void moveTo(double x, double y) {
		pos.setX(x);
		pos.setY(y);
	}

}
