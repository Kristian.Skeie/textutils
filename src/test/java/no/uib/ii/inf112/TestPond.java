package no.uib.ii.inf112;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

import no.uib.ii.inf112.pond.Pond;
import no.uib.ii.inf112.pond.PondObject;
import no.uib.ii.inf112.pond.Position;
import no.uib.ii.inf112.pond.impl.Duck;

public class TestPond {
    
    Pond pond = new Pond(20,20);



    @Test
    void testConstructor(){
        assertFalse(pond.objs.isEmpty()); // should not be empty

    }

    @Test 
    void testAdd(){ 
        PondObject duckObject = new Duck(Position.create(0, 0), 50);
        pond.add(duckObject);
        assertEquals(2, pond.objs.size());

        PondObject duckObject2 = new Duck(Position.create(20,20), 100);
        pond.add(duckObject2);
        assertEquals(3, pond.objs.size());
    }

    @Test
    void testPosition(){
        PondObject duckObject = new Duck(Position.create(1, 2), 50);
        assertEquals(1, duckObject.getX());
        assertEquals(2, duckObject.getY());
        pond.add(duckObject);
        assertEquals(1, duckObject.getX());
        assertEquals(2, duckObject.getY());
        assertEquals(duckObject.getX(), pond.objs.get(1).getX());
    }

    @Test
    void testSize(){
        PondObject duckObject = new Duck(Position.create(1, 2), 50);
        assertEquals(50, duckObject.getSize());
        pond.add(duckObject);
    }

    @Test
    void testMove(){
        pond.objs.get(0).moveTo(10, 1);
        assertEquals(1, pond.objs.get(0).getY()); 
        assertEquals(10, pond.objs.get(0).getX());
    }

    @Test
    void testStep(){
        for(int j = 0; j < 10000; j++){    
            for (int i = 0; i < 24; i++) {
                pond.step();
                assertEquals(j+1, pond.objs.size());
            }
        }
//
        //for (int i = 0; i < 24; i++) {
        //    pond.step();
        //    assertEquals(2, pond.objs.size());
        //}
//
        //for (int i = 0; i < 24; i++) {
        //    pond.step();
        //    assertEquals(3, pond.objs.size());
        //}

        //for (int i = 0; i < 950; i++) {
        //    pond.step();
        //    
        //}
        //assertEquals(40, pond.objs.size());
        //for (int i = 0; i < 23; i++) {
        //    pond.step();
        //    assertEquals(2, pond.objs.size());
        //}
        //pond.step();
        //assertEquals(3, pond.objs.size());
        //assertEquals(1, pond.objs.get(0).getY());
        //assertEquals(1, pond.objs.get(0).getX());

    }

}