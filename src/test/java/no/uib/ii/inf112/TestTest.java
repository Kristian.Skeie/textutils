package no.uib.ii.inf112;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class TestTest {
	TextAligner aligner = new TextAligner() {

		public String center(String text, int width) {
			String textCopy = text.trim(); //removing space in front and back 
			if (0 > width ) 
				throw new IllegalArgumentException("Negative width");
			if (textCopy.length() > width ) 
				throw new IllegalArgumentException("Word longer than width");
			int extra = (width - textCopy.length()) / 2;
			return " ".repeat(extra) + textCopy + " ".repeat(extra);
		}

		public String flushRight(String text, int width) {
			String textCopy = text.trim(); //removing space in front and back
			if (0 > width ) 
				throw new IllegalArgumentException("Negative width");
			if (textCopy.length() > width) 
				throw new IllegalArgumentException("Word longer than width");
			return " ".repeat(width-textCopy.length()) + textCopy;
		}

		public String flushLeft(String text, int width) {
			String textCopy = text.trim(); //removing space in front and back
			if (0 > width ) 
				throw new IllegalArgumentException("Negative width");
			if (textCopy.length() > width)
				throw new IllegalArgumentException("Word longer than width");
			return textCopy + " ".repeat(width-textCopy.length());
		}

		public String justify(String text, int width) {
			String textCopy = text.trim(); //removing space in front and back
			if (0 > width ) 
				throw new IllegalArgumentException("Negative width");
			if (textCopy.length() > width)
				throw new IllegalArgumentException("Word longer than width");
			
			String[] words = textCopy.split(" ");
			if(words.length == 1){
				return aligner.center(textCopy, width);
			}

			int extra = (width - textCopy.length()) / (textCopy.split(" ").length-1);
			String newText = "";
			for (String word : words) {
				newText = newText + word + " ".repeat(extra+1);
			}
			return newText.trim();
		}
	};
		

	@Test
	void testCenter() {
		assertEquals("AAA", aligner.center("AAA", 4));
		assertEquals("  A  ", aligner.center("A", 5));
		assertEquals(" foo ", aligner.center("foo", 5));
		assertEquals(" foo ", aligner.center(" foo", 5));
		assertEquals("  foo  ", aligner.center("   foo ", 8));
		assertEquals(" this is a test ", aligner.center(" this is a test      " , 16));

	}

	@Test 
	void longerThanWord() {
		IllegalArgumentException thrown = assertThrows(
			IllegalArgumentException.class, 
			() -> aligner.center("abc",2),"Expected IllegalArgumentExeption");
			assertTrue(thrown.getMessage().contentEquals("Word longer than width"));

			IllegalArgumentException right = assertThrows(
			IllegalArgumentException.class, 
			() -> aligner.flushRight("abc",2),"Expected IllegalArgumentExeption");
			assertTrue(right.getMessage().contentEquals("Word longer than width"));

			IllegalArgumentException left = assertThrows(
			IllegalArgumentException.class, 
			() -> aligner.flushLeft("abc",2),"Expected IllegalArgumentExeption");
			assertTrue(left.getMessage().contentEquals("Word longer than width"));

			IllegalArgumentException justify = assertThrows(
			IllegalArgumentException.class, 
			() -> aligner.justify("abc",2),"Expected IllegalArgumentExeption");
			assertTrue(justify.getMessage().contentEquals("Word longer than width"));
	}

	@Test
	void negativeWidth() {
		IllegalArgumentException center = assertThrows(
			IllegalArgumentException.class, 
			() -> aligner.center("abc", -2),"Expected IllegalArgumentExeption");
			assertTrue(center.getMessage().contentEquals("Negative width"));

		IllegalArgumentException right = assertThrows(
			IllegalArgumentException.class, 
			() -> aligner.flushRight("abc", -2),"Expected IllegalArgumentExeption");
			assertTrue(right.getMessage().contentEquals("Negative width"));

		IllegalArgumentException left = assertThrows(
			IllegalArgumentException.class, 
			() -> aligner.flushLeft("abc", -2),"Expected IllegalArgumentExeption");
			assertTrue(left.getMessage().contentEquals("Negative width"));

		IllegalArgumentException justify = assertThrows(
				IllegalArgumentException.class, 
				() -> aligner.justify("abc", -2),"Expected IllegalArgumentExeption");
				assertTrue(justify.getMessage().contentEquals("Negative width"));
	}

	
	@Test
	void testflushRight(){
		assertEquals("  AAA", aligner.flushRight("AAA", 5));
		assertEquals("  AAA",aligner.flushRight(" AAA ", 5));
	}

	@Test
	void testflushLeft(){
		assertEquals("A    ", aligner.flushLeft("A",5));
		assertEquals("AAA  ",aligner.flushLeft(" AAA ", 5));
	}

	@Test
	void justify(){
		assertEquals("fee   fie   foo", aligner.justify("fee fie foo", 15));
		assertEquals("fee   fie   foo", aligner.justify(" fee fie foo ", 15));
		assertEquals("fee  fie  foo", aligner.justify("fee fie foo", 13));
		assertEquals("x  x  x  x  x  x", aligner.justify("x x x x x x", 16));
		assertEquals("x    x", aligner.justify("x x", 6));
		assertEquals("  x  ", aligner.justify("x", 5));
	}

} 

